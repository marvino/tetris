# Port of this pygame tetris https://gist.github.com/silvasur/565419
# Copyright (c) 2010 "Laria Carolin Chabowski"<me@laria.me>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

from st3m import logging
from st3m.application import Application
from st3m.input import InputState
from ctx import Context
import leds
import media
import math

log = logging.Log(__name__, level=logging.INFO)
log.info("import")

from random import randrange as rand

# The configuration
config = {
    'cell_size': 6,
    'cols':      8,
    'rows':      16,
    'delay':     750,
    'music_duration': 39000
}

colors = [
(0,   0,   0  ),
(255, 0,   0  ),
(0,   150, 0  ),
(0,   0,   255),
(255, 120, 0  ),
(255, 255, 0  ),
(180, 0,   255),
(0,   220, 220)
]

# Define the shapes of the single parts
tetris_shapes = [
    [[1, 1, 1],
     [0, 1, 0]],
    
    [[0, 2, 2],
     [2, 2, 0]],
    
    [[3, 3, 0],
     [0, 3, 3]],
    
    [[4, 0, 0],
     [4, 4, 4]],
    
    [[0, 0, 5],
     [5, 5, 5]],
    
    [[6, 6, 6, 6]],
    
    [[7, 7],
     [7, 7]]
]

# Define leds that correlate to stone position
y_leds = [
    [32,31,30,29,28,27],
    [8,9,10,11,12,13]
]

x_leds = [
    [36,37,38,39,0,1,2,3,4],
    [22,21,20,19,18]
]


def check_collision(board, shape, offset):
    off_x, off_y = offset
    for cy, row in enumerate(shape):
        for cx, cell in enumerate(row):
            try:
                if cell and board[ cy + off_y ][ cx + off_x ]:
                    return True
            except IndexError:
                return True
    return False



def remove_row(board, row):
    del board[row]
    return [[0 for i in range(config['cols'])]] + board



def join_matrixes(mat1, mat2, mat2_off):
    off_x, off_y = mat2_off
    for cy, row in enumerate(mat2):
        for cx, val in enumerate(row):
            mat1[cy+off_y-1    ][cx+off_x] += val
    return mat1



def new_board():
    board = [ [ 0 for x in range(config['cols']) ]
            for y in range(config['rows']) ]
    board += [[ 1 for x in range(config['cols'])]]
    return board



def rgb_to_hsv(r, g, b):
    r, g, b = r/255.0, g/255.0, b/255.0
    mx = max(r, g, b)
    mn = min(r, g, b)
    df = mx-mn
    if mx == mn:
        h = 0
    elif mx == r:
        h = (60 * ((g-b)/df) + 360) % 360
    elif mx == g:
        h = (60 * ((b-r)/df) + 120) % 360
    elif mx == b:
        h = (60 * ((r-g)/df) + 240) % 360
    if mx == 0:
        s = 0
    else:
        s = (df/mx)
    v = mx
    return math.floor(h), s, v




class TetrisApp(Application):
    def __init__(self, app_ctx) -> None:
        super().__init__(app_ctx)
        self.gameover = False
        self.paused = False
        self._delta_ms_sum = 0
        self.score = 0

        self.stone_id = None
        self.width = config['cell_size']*config['cols']
        self.height = config['cell_size']*config['rows']

        self.block_rotate_left = False
        self.block_rotate_right = False
        self.block_left = False
        self.block_right = False
        self.block_pause = True

        self.stone = self.stone_x = self.stone_y = self.board = False
        self.init_game()
        
        self.music_time_elapsed = 0



    def new_stone(self):
        self.stone_id = rand(len(tetris_shapes))
        self.stone = tetris_shapes[self.stone_id]
        self.stone_x = int(config['cols'] / 2 - len(self.stone[0])/2)
        self.stone_y = 0

        self.set_leds_y()
        self.set_leds_x()

        if check_collision(self.board,
                           self.stone,
                           (self.stone_x, self.stone_y)):
            self.gameover = True
            leds.set_all_rgb(1.0, 0.0, 0.0)
            leds.update()



    def draw_matrix(self, ctx, matrix, offset):
        off_x, off_y  = offset
        for y, row in enumerate(matrix):
            for x, val in enumerate(row):
                if val:
                    red, green, blue = colors[val]
                    ctx.rgb(red, green, blue,
                        ).rectangle(
                            (off_x+x) * config['cell_size'],
                            (off_y+y) * config['cell_size'], 
                            config['cell_size'],
                            config['cell_size'],
                        ).fill()



    def set_leds_y(self):
        # get led that represents the approx height of the current stone
        y_led = len(y_leds[0]) / config['rows'] * self.stone_y
        y_led_higher = math.ceil(y_led)
        y_led_lower = y_led_higher - 1
        y_led_higher_value = y_led - y_led_lower
        y_led_lower_value = y_led_higher - y_led

        # get stone color
        color = colors[self.stone_id + 1]
        hsv = rgb_to_hsv(color[0], color[1], color[2])

        # activate representing led and turn of all others
        for led_set in y_leds:
            for i, led_pos in enumerate(led_set):
                if y_led_lower == i:
                    leds.set_hsv(led_pos, hsv[0], 1.0, y_led_lower_value)
                elif y_led_higher == i:
                    leds.set_hsv(led_pos, hsv[0], 1.0, y_led_higher_value)
                else:
                    leds.set_rgb(led_pos, 0.0, 0.0, 0.0)

        leds.update()



    def set_leds_x(self):
        # get stone color
        color = colors[self.stone_id + 1]
        hsv = rgb_to_hsv(color[0], color[1], color[2])

        # activate representing led and turn of all others
        for led_set in x_leds:
            # get led that represents the approx x of the current stone
            x_led = len(led_set) / config['cols'] * self.stone_x
            x_led_lower = math.ceil(x_led)
            x_led_higher = x_led_lower + 1
            x_led_higher_value = (x_led + 1) - x_led_lower
            x_led_lower_value = x_led_higher - (x_led + 1)

            for i, led_pos in enumerate(led_set):
                if x_led_lower == i:
                    leds.set_hsv(led_pos, hsv[0], 1.0, x_led_lower_value)
                elif x_led_higher == i:
                    leds.set_hsv(led_pos, hsv[0], 1.0, x_led_higher_value)
                else:
                    leds.set_rgb(led_pos, 0.0, 0.0, 0.0)

        leds.update()




    def move(self, delta_x):
        if self.gameover or self.paused:
            return

        new_x = self.stone_x + delta_x
        if new_x < 0:
            new_x = 0
        if new_x > config['cols'] - len(self.stone[0]):
            new_x = config['cols'] - len(self.stone[0])
        if not check_collision(self.board,
                                self.stone,
                                (new_x, self.stone_y)):
            self.stone_x = new_x

        self.set_leds_x()



    def drop(self):
        if self.gameover or self.paused:
            return

        self.stone_y += 1

        self.set_leds_y()

        if not check_collision(self.board,
                                self.stone,
                                (self.stone_x, self.stone_y)):
            return

        self.board = join_matrixes(
            self.board,
            self.stone,
            (self.stone_x, self.stone_y)
        )

        self.new_stone()

        while True:
            for i, row in enumerate(self.board[:-1]):
                if 0 not in row:
                    self.board = remove_row(
                        self.board, i)
                    self.score += 1
                    break
            else:
                break



    def rotate_stone(self, clockwise = True):
        if self.gameover or self.paused:
            return

        shape = self.stone

        if clockwise:
            shape_range_x = range(len(shape[0]))
            shape_range_y = range(len(shape) -1, -1, -1)
        else:
            shape_range_x = range(len(shape[0]) - 1, -1, -1)
            shape_range_y = range(len(shape))
 
        new_stone = []

        for x in shape_range_x:
            new_row = []
            for y in shape_range_y:
                new_row.append(shape[y][x])
            new_stone.append(new_row)

        if not check_collision(self.board,
                                new_stone,
                                (self.stone_x, self.stone_y)):
            self.stone = new_stone



    def toggle_pause(self):
        self.paused = not self.paused



    def start_game(self):
        if self.gameover is True:
            self.init_game()
            self.gameover = False
        else:
            self.toggle_pause()



    def init_game(self):
        self.board = new_board()
        self.new_stone()
        self.score = 0
        leds.set_all_rgb(0.0, 0.0, 0.0)
        leds.update()
        self.load_music()



    def load_music(self):
        media.stop()
        media.load("/flash/sys/apps/iron-bound-tetris/tetris.mp3")
        self.music_time_elapsed = 0




    def move_or_skip(self, delta_ms, delay):
        self._delta_ms_sum += delta_ms
        if self._delta_ms_sum > delay:
            self._delta_ms_sum = 0
            return True

        return False



    def draw(self, ctx: Context) -> None:
        # Paint the background black
        ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 240).fill()

        # Game board
        ctx.scale(2, 2).translate(-25, -50)
        if self.paused or self.gameover:
            ctx.rgb(0.5, 0.5, 0.5).rectangle(0, 0, self.width, self.height).stroke()
        else:
            ctx.rgb(1, 1, 1).rectangle(0, 0, self.width, self.height).stroke()
        
        # Draw the game
        self.draw_matrix(ctx, self.board, (0, 0))
        self.draw_matrix(ctx, self.stone,
                            (self.stone_x,
                            self.stone_y))

        # rescale to normal
        ctx.scale(0.5, 0.5).translate(50, 100)

        # print gameover and pause screens
        if self.gameover or self.paused:
            ctx.text_align = ctx.CENTER
            ctx.text_baseline = ctx.MIDDLE
            ctx.font_size = 30
            ctx.rgb(1, 1, 1)
            ctx.move_to(0, 0)
            ctx.save()
            if self.gameover:
                text = "GAME OVER"
            elif self.paused:
                text = "PAUSED"
            ctx.text(text)
            ctx.restore()

        #show score
        ctx.text_align = ctx.RIGHT
        ctx.text_baseline = ctx.MIDDLE
        ctx.rgb(1, 1, 1)
        ctx.font_size = 15
        ctx.move_to(100,-10)
        ctx.text("Score:")
        ctx.move_to(100,10)
        ctx.text(f"{self.score}")



    def think(self, ins: InputState, delta_ms: int) -> None:
        self.music_time_elapsed += delta_ms
        if self.music_time_elapsed > config['music_duration']:
            self.load_music()
        else:
            media.think(delta_ms)

        super().think(ins, delta_ms)

        self.input.think(ins, delta_ms)

        # upper top left petal is used to rotate TODO top left rotates left and top right rotates rightt
        if ins.captouch.petals[8].pressed and not self.block_rotate_left:
            try:
                self.rotate_stone(clockwise=False)
            except Exception as e:
                print(e)
            self.block_rotate_left = True

        # top right petal rotates right
        elif ins.captouch.petals[2].pressed and not self.block_rotate_right:
            self.rotate_stone(clockwise=True)
            self.block_rotate_right = True


        # bottom left and right (upper) petals are used for left and righ
        elif ins.captouch.petals[6].pressed and not self.block_left:
            self.move(-1)
            self.block_left = True
        elif ins.captouch.petals[4].pressed and not self.block_right:
            self.move(1)
            self.block_right = True

        # lower bottom patel is used to speed up drop
        elif ins.captouch.petals[5].pressed and self.move_or_skip(delta_ms, (config['delay'] / 10)):
            self.drop()
    
        # top middle starts a new game or toggles pause
        elif ins.captouch.petals[0].pressed and not self.block_pause:
            self.start_game()
            self.block_pause = True

        # unblock
        if not ins.captouch.petals[8].pressed:
            self.block_rotate_left = False
        if not ins.captouch.petals[2].pressed:
            self.block_rotate_right = False
        if not ins.captouch.petals[6].pressed:
            self.block_left = False
        if not ins.captouch.petals[4].pressed:
            self.block_right = False
        if not ins.captouch.petals[0].pressed:
            self.block_pause = False

        # Tick to move the block down
        if not self.move_or_skip(delta_ms, config['delay']):
            return

        self.drop()



    def on_exit(self):
        super().on_exit()
        media.stop()





# For running with `mpremote run`:
if __name__ == "__main__":
    import st3m.run

    st3m.run.run_app(TetrisApp)
