# tetris

A tetris game for the 2023 camps flow3r badge


- lower (upper) petals to move right and left
- bottom petal to move down more quickly
- top petal to pause / start new game
- upper left and right petals to rotate stone clockwise / counter clockwise

## authors
Port of this pygame tetris https://gist.github.com/silvasur/565419  
Copyright (c) 2010 "Laria Carolin Chabowski"<me@laria.me>  
  
port to flow3r badge by Andrew B. / iron-bound  
https://git.flow3r.garden/iron-bound/tetris

enhancements by marvino